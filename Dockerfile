# Use an official Python runtime as a parent image
FROM node:8

# Set the working directory to /app
WORKDIR /app

COPY package.json /app
RUN npm install
# Copy the current directory contents into the container at /app
# ADD . /app
COPY . /app


# Make port 80 available to the world outside this container
EXPOSE 3000

# Define environment variable
ENV NAME World

# Run app.py when the container launches
# CMD ["python", "app.py"]
CMD ["node", "index.js"]